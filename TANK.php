<!DOCTYPE html>
<html>
<head>

<title>TANK LEVELS</title>

<style>
table
{
    border: 1px solid black;
}

col 
{
    display: table-column;
} 
</style>
</head>

<body style="background-color:#8F0F30;">

<img src="Basic_logo.gif" alt="Western Sydney University">

<p>Please enter the tank levels for each vessel and then click "Enter Values" button to save:</p>


<form action="process5.php" method="post">

Date: <input type="Date" name="Date"> <br><br>

<table>

<tr>	
	<td>Vessel 1 Kpa<input type="number" name="Kpa_1" min="0" max="10000" step="1" value="17"></td> 
</tr>
<tr>	
	<td>Vessel 1 mm H2O<input type="number" name="mmH2O_1" min="0" max="10000" step="1"> </td> 
</tr>

<tr>	
	<td>Vessel 2 Kpa<input type="number" name="Kpa_2" min="0" max="10000" step="1" value="17"></td> 
</tr>
<tr>	
	<td>Vessel 2 mm H2O<input type="number" name="mmH2O_2" min="0" max="10000" step="1"> </td> 
</tr>

<tr>	
	<td>Vessel 3 Kpa<input type="number" name="Kpa_3" min="0" max="10000" step="1" value="17"></td> 
</tr>
<tr>	
	<td>Vessel 3 mm H2O<input type="number" name="mmH2O_3" min="0" max="10000" step="1"> </td> 
</tr>
 </table> 
  
<br><br>
<input type="submit" name="formSubmit" value="Enter Values">

</form>


</body>
</html>
