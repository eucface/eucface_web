<!DOCTYPE html>
<html>
<head>

<title>CAL PAGE</title>

<style>
table
{
    border: 1px solid black;
}

col 
{
    display: table-column;
} 
</style>
</head>

<body style="background-color:#8F0F30;">

<img src="Basic_logo.gif" alt="Western Sydney University">

<p>Please enter the Calibration values and then click "Enter Values" to submit the form:</p>


<form action="process3.php" method="post">

Date of Calibration <input type="date" name="date"> <br><br>
Calibrated By: <br> 
    <input type="radio" name="user" value="Craig"> Craig<br>
    <input type="radio" name="user" value="Vinod"> Vinod<br>
	<input type="radio" name="user" value="C and V"> C and V<br>
    <input type="radio" name="user" value="Other"> Other<br><br><br>
<table>
<tr>
	<td>IRGA S/No: HGA - 
	<select name="R1_IRGA">
    <option value="1639">1639</option>
    <option value="1640">1640</option>
    <option value="1641">1641</option>
    <option value="1642">1642</option>
    <option value="1643">1643</option>
    <option value="1644">1644</option>
    <option value="1645">1645</option>
    <option value="1646">1646</option>
	  <option selected="1647">1647</option>
    <option value="1648">1648</option>
    <option value="1649">1649</option>
    <option value="1650">1650</option>
	  <option value="1661">1661</option>
    <option value="1662">1662</option>
    <option value="3290">3290</option>
	</select>
	</td>	
</tr>
<tr>	
	<td>R1 Zero: <input type="number" name="R1_0" min="-50" max="1000" step="0.5" value="-1"> </td> 
</tr>
<tr>	
	<td>R1 Span:<input type="number" name="R1_SP" value="470"> </td>
</tr>
<tr>  
	<td>IRGA S/No: HGA - 
	<select name="R1MP_IRGA">
    <option value="1639">1639</option>
    <option value="1640">1640</option>
    <option value="1641">1641</option>
    <option value="1642">1642</option>
    <option value="1643">1643</option>
    <option selected="1644">1644</option>
    <option value="1645">1645</option>
    <option value="1646">1646</option>
	  <option value="1647">1647</option>
    <option value="1648">1648</option>
    <option value="1649">1649</option>
    <option value="1650">1650</option>
	  <option value="1661">1661</option>
    <option value="1662">1662</option>
    <option value="3290">3290</option>
	</select>
	</td> 
</tr>
<tr>	
	<td>R1 MP Zero:<input type="number" name="R1MP_0" min="-50" max="1000" step="0.5" value="-1"> </td>
</tr>
<tr> 	
	<td>R1 MP Span:<input type="number" name="R1MP_SP" value="470"> </td>
</tr>
	<td><br></td>
<tr>
	<td>IRGA S/No: HGA - 
	<select name="R2_IRGA">
    <option value="1639">1639</option>
    <option value="1640">1640</option>
    <option value="1641">1641</option>
    <option selected="1642">1642</option>
    <option value="1643">1643</option>
    <option value="1644">1644</option>
    <option value="1645">1645</option>
    <option value="1646">1646</option>
	  <option value="1647">1647</option>
    <option value="1648">1648</option>
    <option value="1649">1649</option>
    <option value="1650">1650</option>
	  <option value="1661">1661</option>
    <option value="1662">1662</option>
    <option value="3290">3290</option>
	</select>
	</td> 
</tr>
<tr>
	<td>R2 Zero:<input type="number" name="R2_0" min="-50" max="1000" step="0.1" value="-1"> </td>
</tr>
<tr> 	
	<td>R2 Span:<input type="number" name="R2_SP" value="470"> </td>
</tr>
	<td><br></td>
<tr>
	<td>IRGA S/No: HGA - 
	<select name="R3_IRGA">
    <option value="1639">1639</option>
    <option value="1640">1640</option>
    <option value="1641">1641</option>
    <option value="1642">1642</option>
    <option value="1643">1643</option>
    <option value="1644">1644</option>
    <option value="1645">1645</option>
    <option value="1646">1646</option>
	  <option value="1647">1647</option>
    <option selected="1648">1648</option>
    <option value="1649">1649</option>
    <option value="1650">1650</option>
	  <option value="1661">1661</option>
    <option value="1662">1662</option>
    <option value="3290">3290</option>
	</select>
	</td> 
</tr>
<tr>
	<td>R3 Zero:<input type="number" name="R3_0" min="-50" max="1000" step="0.5" value="-1"></td> 
</tr>
<tr> 	
	<td>R3 Span:<input type="number" name="R3_SP" value="470"> </td>
</tr>
	<td><br></td>
<tr>
	<td>IRGA S/No: HGA - 
	<select name="R4_IRGA">
    <option value="1639">1639</option>
    <option value="1640">1640</option>
    <option value="1641">1641</option>
    <option value="1642">1642</option>
    <option value=1643">1643</option>
    <option value="1644">1644</option>
    <option selected="1645">1645</option>
    <option value="1646">1646</option>
	  <option value="1647">1647</option>
    <option value="1648">1648</option>
    <option value="1649">1649</option>
    <option value="1650">1650</option>
	  <option value="1661">1661</option>
    <option value="1662">1662</option>
    <option value="3290">3290</option>
	</select>
	</td>
</tr>
<tr>	
	<td>R4 Zero:<input type="number" name="R4_0" min="-50" max="1000" step="0.5" value="-1"></td> 
</tr>
<tr> 
	<td>R4 Span:<input type="number" name="R4_SP" value="470"> </td>
 </tr>
 <tr> 
	<td>IRGA S/No: HGA - 
	<select name="R4MP_IRGA">
    <option value="1639">1639</option>
    <option value="1640">1640</option>
    <option value="1641">1641</option>
    <option value="1642">1642</option>
    <option value="1643">1643</option>
    <option value="1644">1644</option>
    <option value="1645">1645</option>
    <option value="1646">1646</option>
	  <option value="1647">1647</option>
    <option value="1648">1648</option>
    <option value="1649">1649</option>
    <option value="1650">1650</option>
	  <option value="1661">1661</option>
    <option value="1662">1662</option>
    <option selected="3290">3290</option>
	</select>
	</td> 
</tr>
<tr>	
	<td>R4 MP Zero:<input type="number" name="R4MP_0" min="-50" max="1000" step="0.5" value="-1"> </td>
</tr>
<tr> 
	<td>R4 MP Span:<input type="number" name="R4MP_SP" value="470"> </td>
</tr>
	<td><br></td>
<tr>	
	<td>IRGA S/No: HGA - 
	<select name="R5_IRGA">
    <option value="1639">1639</option>
    <option value="1640">1640</option>
    <option value="1641">1641</option>
    <option value="1642">1642</option>
    <option value="1643">1643</option>
    <option value="1644">1644</option>
    <option value="1645">1645</option>
    <option selected="1646">1646</option>
	  <option value="1647">1647</option>
    <option value="1648">1648</option>
    <option value="1649">1649</option>
    <option value="1650">1650</option>
	  <option value="1661">1661</option>
    <option value="1662">1662</option>
    <option value="3290">3290</option>
	</select>
	</td>
</tr>
<tr>
	<td>R5 Zero:<input type="number" name="R5_0" min="-50" max="1000" step="0.5" value="-1"></td> 
</tr>
<tr> 
	<td>R5 Span:<input type="number" name="R5_SP" value="470"> </td>
</tr>
<tr>	
	<td>IRGA S/No: HGA - 
	<select name="R5MP_IRGA">
    <option selected="1639">1639</option>
    <option value="1640">1640</option>
    <option value="1641">1641</option>
    <option value="1642">1642</option>
    <option value="1643">1643</option>
    <option value="1644">1644</option>
    <option value="1645">1645</option>
    <option value="1646">1646</option>
	  <option value="1647">1647</option>
    <option value="1648">1648</option>
    <option value="1649">1649</option>
    <option value="1650">1650</option>
	  <option value="1661">1661</option>
    <option value="1662">1662</option>
    <option value="3290">3290</option>
	</select>
	</td>
</tr>
<tr>	
	<td>R5 MP Zero:<input type="number" name="R5MP_0" min="-50" max="1000" step="0.5" value="-1"></td>
</tr>
<tr>	
	<td>R5 MP Span:<input type="number" name="R5MP_SP" value="470"> </td>
</tr>
	<td><br></td>
<tr>	
	<td>IRGA S/No: HGA - 
	<select name="R6_IRGA">
    <option value="1639">1639</option>
    <option selected="1640">1640</option>
    <option value="1641">1641</option>
    <option value="1642">1642</option>
    <option value="1643">1643</option>
    <option value="1644">1644</option>
    <option value="1645">1645</option>
    <option value="1646">1646</option>
  	<option value="1647">1647</option>
    <option value="1648">1648</option>
    <option value="1649">1649</option>
    <option value="1650">1650</option>
	  <option value="1661">1661</option>
    <option value="1662">1662</option>
    <option value="3290">3290</option>
	</select>
	</td> 
</tr>
<tr>	
	<td>R6 Zero: <input type="number" name="R6_0" min="-50" max="1000" step="0.5" value="-1"></td>
</tr>
<tr>	
	<td>R6 Span:<input type="number" name="R6_SP" value="470"> </td>
 </tr>
 </table> 
  
<br><br>
<input type="submit" name="formSubmit" value="Enter Values">

</form>


</body>
</html>
